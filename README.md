# Muistipeli

Very simple game example in javaScript for [Taitotalo](https://taitotalo.fi/)

## License
MIT license. This project is based on [https://code-boxx.com/simple-memory-game-javascript/](https://code-boxx.com/simple-memory-game-javascript/)
