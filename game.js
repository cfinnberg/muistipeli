var game = {
    htmlGameArea: null, // The gameArea element (that contains the images)
    htmlCounter: null,  // The movesCounter element
    htmlCards: [], // Array of img -elements
    grid: [],   // Array of cards (numbers from 1 to maxPairs)
    imageURL: "kuvat/smiley-POS.png",
    lastCardIndex: null,    // The previous card index (in grid)
    matchesNumber: 0,   // Number of matches so far
    movesNumber: 0,     // Number of moves so far
    pairsNumber: 2,     // Number of pairs in this round
    maxPairs: 6,        // Maximum number of pairs

    // This is executed just once, when the page is loaded
    init: () => {
        game.htmlGameArea = document.getElementById('gameArea');
        game.htmlCounter = document.getElementById('movesCounter');

        // Create the images html elements and save them in htmlCards.
        for (let i=0; i<game.maxPairs*2; i++) {
            let hcard = document.createElement('img');
            hcard.dataset.idx = i;
            hcard.classList.add('card');
            game.htmlCards.push(hcard);
        }
        // Start matchInit to start a round
        game.matchInit();
    },

    // This is executed every time the game is reseted (every round)
    matchInit: () => {
        // Create a new grid
        game.grid = [];
        // newGrid is a temporal variable with all pairs
        let newGrid = [];
        for (let i=1; i<=game.pairsNumber; i++) {
            newGrid.push(i);
            newGrid.push(i);
        }
        // Save the new grid in random order to shuffle the cards of the play
        while (newGrid.length) {
            game.grid.push(newGrid.splice(Math.floor(newGrid.length * Math.random()),1)[0]);
        }

        // Initialize the gameArea and add as many images as this round allows
        game.htmlGameArea.innerHTML = "";
        for (let i=0; i<game.pairsNumber*2; i++) {
            hcard = game.htmlCards[i];
            hcard.src = game.imageURL.replace('POS', '0');
            hcard.onclick = game.openCard;
            hcard.classList.remove('right');
            game.htmlGameArea.appendChild(hcard);
        }
        // Initialize some variables and update movesCounter in the webpage
        game.matchesNumber = 0;
        game.movesNumber = 0;
        game.htmlCounter.innerText = game.movesNumber;
    },

    // This is executed every time a card is clicked
    openCard: (event) => {
        // Get the element that was clicked and the data of the card
        let htmlCardNow = event.target;
        let idx = htmlCardNow.dataset['idx'];
        let cardNow = game.grid[idx];
        htmlCardNow.src = game.imageURL.replace('POS', cardNow);    // Update the image to the actual image
        htmlCardNow.classList.add('open'); // Add 'open' class to the element (yellow background)
        htmlCardNow.onclick = null;        // Disable click event, so it can't be clicked while waiting for the next card
        
        if (game.lastCardIndex == null) {   // If there was no previous card saved, save the (index of the) card as previous card
            game.lastCardIndex = idx;
            // And exit. Nothing else to do
        } else { // This is the second selected card.

            // Update number of moves.
            game.movesNumber++; 
            game.htmlCounter.innerText = game.movesNumber;
            
            // Get data of the previous card
            let cardLast = game.grid[game.lastCardIndex];
            let htmlCardLast = game.htmlCards[game.lastCardIndex];
            
            // And reset some variables and properties
            game.lastCardIndex = null;
            htmlCardNow.classList.remove('open');
            htmlCardLast.classList.remove('open');
            
            // Check what's the situation: new card == last card?
            if (cardNow == cardLast) {  // There is a match!
                // Update cards background (green)
                htmlCardNow.classList.add('right');
                htmlCardLast.classList.add('right');

                // Update matches number and check if this was the last remainin match
                game.matchesNumber++;
                if (game.matchesNumber == game.pairsNumber) {
                    // Show a message to the user. Use a timer, so it gives time to the openCard function to end and finalize the changes.
                    setTimeout(() => {
                        alert("Voitit pelin.");
                        if ( game.pairsNumber < game.maxPairs) { game.pairsNumber += 1; } // Add a pair (if possible) to the next round.
                        game.matchInit(); // Start the next round
                    }, 10);
                }
            } else {  // There is no match
                // Update cards background (red)
                htmlCardNow.classList.add('wrong');
                htmlCardLast.classList.add('wrong');

                // Set a timer to reset the cards. 1000ms = 1seg.
                setTimeout(() => {
                    // Remove background color
                    htmlCardNow.classList.remove('wrong');
                    htmlCardLast.classList.remove('wrong');

                    // Reset cards image to default
                    htmlCardNow.src = game.imageURL.replace('POS', '0');
                    htmlCardLast.src = game.imageURL.replace('POS', '0');

                    // Reset onclick event for the card
                    htmlCardNow.onclick = game.openCard;
                    htmlCardLast.onclick = game.openCard;
                }, 1000);
            }
        }
    }
}

// Execute the game initialization when the DOM is fully loaded
window.addEventListener('DOMContentLoaded', game.init)
